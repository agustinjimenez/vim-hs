" indentation for cabal
"
" Fork of Haskell-vim
" Original Repository author: raichoo (raichoo@googlemail.com)
" See: github.com/neovimhaskell/haskell-vim

if !exists('g:cabal_indent_section')
  "executable name
  ">>main-is:           Main.hs
  ">>hs-source-dirs:    src
  let g:cabal_indent_section = 4
endif

setlocal indentexpr=GetCabalIndent()
setlocal indentkeys=!^F,o,O,<CR>

function! GetCabalIndent()
  let l:prevline = getline(v:lnum - 1)

  if l:prevline =~ '\C^\(executable\|library\|flag\|source-repository\|test-suite\|benchmark\)'
    return g:cabal_indent_section
  else
    return match(l:prevline, '\S')
  endif
endfunction
